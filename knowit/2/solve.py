from math import radians, cos, sin, asin, sqrt
import geopy.distance
import csv

def dist(lat1, long1, lat2, long2):
    lat1, long1, lat2, long2 = map(radians, [lat1, long1, lat2, long2])
    dlon = long2 - long1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    distkm = 6371 * c
    return distkm

list = []
lat1 = 0
long1 = 0
g = 0
wc = 3435
total = 0
cities = []
f = open("./cities_short.csv", "r")
reader = csv.reader(f)
for row in reader:
    cities.append(row)
wc = len(cities)
lat1 = 90
long1 = 0
g=0
total=0
for reise in range(wc):
    current_distance = 99999999999999999999
    for city in cities:
        point = city[1][6:-1]
        t = point.split()
        lat2 = float(t[1])
        long2 = float(t[0])
        distance = abs(dist(lat1, long1, lat2, long2))
        #distance = geopy.distance.distance((lat1, long1), (lat2, long2)).km
        if current_distance > distance:
           # print(f"Closest city is {distance} away: {city}")
            current_distance = distance
            cur_lat = lat2
            cur_long = long2
            cur_city = city
    total += current_distance
    print(f"{int(g/wc * 100)}%: {g}/{wc} {wc-g} {round(total)} {int(current_distance)} km to {cur_city}")
    lat1 = cur_lat
    long1 = cur_long
    cities.remove(cur_city)
    g += 1

distance = abs(dist(lat1, long1, 90, 0))
total += distance
print(f"{int(g/wc * 100)}%: {g}/{wc} {wc-g} {round(total)} {int(current_distance)} km to North Pole")





#    i=0
#    f = open("./cities.csv", "r")
#    reader = csv.reader(f)
#    row=0
#    for row in reader:
#        if i in list:
#            i += 1
#            continue
#        else:
#            point = row[1][6:-1]
#            t = point.split()
#            lat2 = float(t[0])
#            long2 = float(t[1])
#            distance = abs(dist(lat1, long1, lat2, long2))
#            if current_distance > distance:
#                current_distance = distance
#                distance_index = i
#                city = row[0]
#                city_index = i
#                cur_lat = lat2
#                cur_long = long2
#            i += 1
#    total += current_distance
#    print(f"{int(g/wc * 100)}%: {g}/{wc} {wc-g} {round(total)} {int(current_distance)} km to {city}")
#    lat1 = cur_lat
#    long1 = cur_long
#    list.append(city_index)
#    g += 1
#
#distance = abs(dist(lat1, long1, 0, 0))
#total += distance
#print(f"{int(g/wc * 100)}%: {g}/{wc} {wc-g} {round(total)} {int(current_distance)} km to North Pole")
