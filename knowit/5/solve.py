f = open("tree.txt", "r")
tree = f.read()
generation = 0
max_gen = 0
for c in tree:
    if c == "(":
        generation += 1
    elif c == ")":
        generation -= 1
    if generation > max_gen:
        max_gen = generation
        print(f"Generation: {generation}")
